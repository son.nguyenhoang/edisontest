**HOW TO BUILD.**

This project was developed on Android studio 4.0.0.

1/ Checkout from git.

2/ Open Android Studio. 

3/ Select File -> New -> Import Project (Use Open project instead of Import may cause the error).

4/ Wait for gradle sync success.

==============================================

This Project follow "Android Architecture Patterns Model-View-ViewModel"
The Unit Test will focus on ViewModel class which should handle the application bussiness.

![Demo](https://media.giphy.com/media/TGihjfC4ggs241VkLD/giphy.gif)
