package com.example.edisontest

import android.os.Bundle
import com.example.edisontest.ui.main.base.BaseActivity
import com.example.edisontest.ui.main.ui.productList.ProductListFragment

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            addFragment(ProductListFragment.newInstance())
        }
    }
}