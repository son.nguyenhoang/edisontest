package com.example.edisontest.ui.main.base

import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.example.edisontest.R

open class BaseActivity : AppCompatActivity() {

    fun getApp(): MyApplication {
        return application as MyApplication
    }


    fun addFragment(fragment: BaseFragment, addToBackStack: Boolean = false) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.apply {
            this.setCustomAnimations(
                R.anim.enter_from_right,
                R.anim.exit_to_left,
                R.anim.enter_from_left,
                R.anim.exit_to_right
            )
            this.add(R.id.container, fragment)
            if (addToBackStack) this.addToBackStack(null)
            this.commitAllowingStateLoss()
        }
    }

    fun replaceFragment(fragment: BaseFragment) {
        supportFragmentManager.beginTransaction().apply {
            this.replace(R.id.container, fragment).addToBackStack(null).commitAllowingStateLoss()
        }
    }

    fun popFragment() {
        supportFragmentManager.popBackStack()
    }

    fun showBackButton() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    fun hideBackButton() {
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
    }

    fun setTitle(title: String) {
        supportActionBar?.title = title
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                popFragment()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}