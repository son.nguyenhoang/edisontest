package com.example.edisontest.ui.main.ui.productList

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.example.edisontest.R
import com.example.edisontest.ui.main.base.BaseFragment
import com.example.edisontest.ui.main.model.Product
import com.example.edisontest.ui.main.model.callback.ProductCallback
import com.example.edisontest.ui.main.ui.detail.ProductDetail
import com.example.edisontest.ui.main.utils.ItemDecorationAlbumColumns
import kotlinx.android.synthetic.main.main_fragment.*
import org.koin.android.viewmodel.ext.android.viewModel


class ProductListFragment : BaseFragment(), ProductCallback {

    companion object {
        fun newInstance() =
            ProductListFragment()
    }

    private val viewModel by viewModel<ProductsViewModel>()
    private var rvAdapter: ProductRvAdapter? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.products.observe(viewLifecycleOwner, Observer {
            it?.let {
                notifyAdapter(it)
            }
        })
        viewModel.fetchProducts()
        baseActivity()?.hideBackButton()
    }

    private fun notifyAdapter(dataSource: ArrayList<Product>) {
        if (rvAdapter == null) {
            rvAdapter = ProductRvAdapter(dataSource, this)
            rv.apply {
                layoutManager = GridLayoutManager(context, 2)
                addItemDecoration(
                    ItemDecorationAlbumColumns(
                        2,
                        40, true
                    )
                )
                adapter = rvAdapter
            }
        } else {
            rvAdapter?.notifyDataSetChanged()
        }
    }

    override fun onProductClick(product: Product, pos: Int) {
        baseActivity()?.addFragment(ProductDetail.newInstance(pos), true)
    }

}