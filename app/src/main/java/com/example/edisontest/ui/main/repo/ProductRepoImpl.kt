package com.example.edisontest.ui.main.repo

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.edisontest.ui.main.model.Product
import com.example.edisontest.ui.main.utils.Utils
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken


class ProductRepoImpl(private val appContext: Context) : ProductRepo {

    private var liveProductList = MutableLiveData<ArrayList<Product>>()

    override fun getProducts(): LiveData<ArrayList<Product>> {
        return liveProductList
    }

    override fun fetchProducts() {
        // simulate with coroutine
        val json = Utils.getJsonDataFromAsset(appContext, "products.json")
        val type = object : TypeToken<ArrayList<Product>>() {}.type
        val data = Gson().fromJson<ArrayList<Product>>(json, type)
        liveProductList.postValue(data)
    }

    override fun like(productId: Int) {
        liveProductList.value?.let {
            val product = it[productId]
            product.like = true
            notifyData()
        }

    }

    override fun unLike(productId: Int) {
        liveProductList.value?.let {
            val product = it[productId]
            product.like = false
            notifyData()
        }
    }

    private fun notifyData() {
        liveProductList.postValue(liveProductList.value)
    }
}