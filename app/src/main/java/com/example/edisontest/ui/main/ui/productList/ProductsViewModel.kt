package com.example.edisontest.ui.main.ui.productList

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.edisontest.ui.main.repo.ProductRepo
import com.example.edisontest.ui.main.model.Product

class ProductsViewModel(var repo: ProductRepo) : ViewModel() {
    var products: LiveData<ArrayList<Product>> = repo.getProducts()

    fun fetchProducts(): LiveData<ArrayList<Product>> {
        repo.fetchProducts()
        return products
    }

    fun like(liking: Boolean, productId: Int) {
        products.value?.let {
            if (liking) {
                repo.like(productId)
            } else {
                repo.unLike(productId)
            }
        }
    }
}