package com.example.edisontest.ui.main.di

import com.example.edisontest.ui.main.repo.ProductRepo
import com.example.edisontest.ui.main.ui.productList.ProductsViewModel
import com.example.edisontest.ui.main.repo.ProductRepoImpl
import org.koin.android.ext.koin.androidContext
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {
    single { ProductRepoImpl(androidContext()) as ProductRepo}
    viewModel { ProductsViewModel(get()) }
}