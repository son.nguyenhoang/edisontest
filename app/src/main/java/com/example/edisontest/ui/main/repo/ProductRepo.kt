package com.example.edisontest.ui.main.repo

import androidx.lifecycle.LiveData
import com.example.edisontest.ui.main.model.Product

interface ProductRepo {
    fun getProducts(): LiveData<ArrayList<Product>>
    fun fetchProducts()
    fun like(productId: Int)
    fun unLike(productId: Int)
}