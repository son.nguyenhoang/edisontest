package com.example.edisontest.ui.main.model

data class Product(val title: String, val imageURL: String, val author: String, var like: Boolean = false)