package com.example.edisontest.ui.main.base

import androidx.fragment.app.Fragment
import com.example.edisontest.R

open class BaseFragment : Fragment() {

    fun baseActivity(): BaseActivity? {
        return activity as? BaseActivity
    }

}