package com.example.edisontest.ui.main.ui.productList

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.edisontest.R
import com.example.edisontest.ui.main.model.Product
import com.example.edisontest.ui.main.model.callback.ProductCallback
import com.squareup.picasso.Picasso

class ProductRvAdapter(private val dataSource: ArrayList<Product>, var callback: ProductCallback) :
    RecyclerView.Adapter<ProductViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ProductViewHolder(inflater.inflate(R.layout.item_product, parent, false))
    }

    override fun getItemCount(): Int {
        return dataSource.count()
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        holder.onBind(dataSource[position], position, callback)
    }

}

class ProductViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    private var image: ImageView = view.findViewById(R.id.imageView)
    var title: TextView = view.findViewById(R.id.tvTitle)
    var author: TextView = view.findViewById(R.id.author)
    var imgLike: ImageView = view.findViewById(R.id.imgLike)

    fun onBind(product: Product, pos: Int, callback: ProductCallback) {
        author.text = "N/A"
        Picasso.get()
            .load(product.imageURL)
            .placeholder(R.drawable.img_holder)
            .error(R.drawable.img_error)
            .into(image)
        title.text = product.title
        author.text = product.author

        var icLike = R.drawable.ic_like
        if (!product.like) {
            icLike = R.drawable.ic_unlik
        }
        Picasso.get()
            .load(icLike)
            .into(imgLike)
        itemView.setOnClickListener {
            callback.onProductClick(product, pos)
        }
    }
}