package com.example.edisontest.ui.main.ui.detail

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.lifecycle.Observer
import com.example.edisontest.R
import com.example.edisontest.ui.main.base.BaseFragment
import com.example.edisontest.ui.main.model.Product
import com.example.edisontest.ui.main.ui.productList.ProductsViewModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_product_detail.*
import kotlinx.android.synthetic.main.fragment_product_detail.imageView
import kotlinx.android.synthetic.main.item_product.*
import kotlinx.android.synthetic.main.item_product.author
import org.koin.android.viewmodel.ext.android.viewModel

private const val PRODUCT_ID = "PRODUCT_ID"

class ProductDetail : BaseFragment() {
    private var productId: Int? = null
    private val viewModel by viewModel<ProductsViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            productId = it.getInt(PRODUCT_ID)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_product_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loadData()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        activity?.onBackPressedDispatcher?.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                baseActivity()?.popFragment()
            }
        })
        baseActivity()?.showBackButton()
    }
    private fun loadData() {
        productId?.let { id ->
            val product: Product? = viewModel.products.value?.get(id)
            product?.let { pr ->
                val title = "${product.title.substring(0, 10)}..."
                baseActivity()?.setTitle(title)
                Picasso.get()
                    .load(pr.imageURL)
                    .placeholder(R.drawable.img_holder)
                    .error(R.drawable.img_error)
                    .into(imageView)
                tvName.text = pr.title
                author.text = pr.author
                updateLikeBtn(pr.like)
                btnLike.setOnClickListener {
                    val liked = !pr.like
                    viewModel.like(liked, id)
                    updateLikeBtn(liked)
                }
                viewModel.products.observe(viewLifecycleOwner, Observer {
                    updateLikeBtn(product.like)
                })
            }
        }


    }

    private fun updateLikeBtn(liked: Boolean) {
        if (liked) {
            btnLike.setBackgroundColor(resources.getColor(R.color.colorPrimary))
            btnLike.text = getString(R.string.lb_liked)
        } else {
            btnLike.setBackgroundColor(resources.getColor(R.color.unlike))
            btnLike.text = getString(R.string.lb_unLike)
        }

    }

    override fun onStop() {
        super.onStop()
        baseActivity()?.hideBackButton()
        baseActivity()?.setTitle(getString(R.string.app_name))
    }
    companion object {
        @JvmStatic
        fun newInstance(productId: Int) =
            ProductDetail().apply {
                arguments = Bundle().apply {
                    putInt(PRODUCT_ID, productId)
                }
            }
    }
}