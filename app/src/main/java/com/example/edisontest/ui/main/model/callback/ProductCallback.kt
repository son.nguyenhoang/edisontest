package com.example.edisontest.ui.main.model.callback

import com.example.edisontest.ui.main.model.Product

interface ProductCallback {
    fun onProductClick(product: Product, pos: Int)
}