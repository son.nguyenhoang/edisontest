package com.example.edisontest

import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.edisontest.BuildConfig.DEBUG
import com.example.edisontest.ui.main.di.appModule
import com.example.edisontest.ui.main.ui.productList.ProductsViewModel

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*
import org.junit.Rule
import org.koin.test.KoinTest
import org.koin.test.KoinTestRule
import org.koin.test.inject
import java.util.logging.Level

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ProductViewModelInstrumentedTest : KoinTest {
    private val model by inject<ProductsViewModel>()

    @Test
    fun testLikeProduct() {
        model.fetchProducts()
        model.products.value?.let {
            val productId = 0
            val product = it[productId]
            model.like(true, productId)
            assertEquals("Liked Product", true, product.like)
            model.like(false, productId)
            assertEquals("Unlike Product", false, product.like)
        }
    }
}